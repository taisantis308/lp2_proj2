<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pagina extends CI_Controller{

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
    }

    /**
     * Chama as views da página Index (principal)
     */
    public function index(){
        $this->load->view('include/cabecalho');
        $this->load->view('include/menu');
        $dados['titulo'] = 'Projeto 2 - LP2';
        $this->load->view('home/home', $dados);   
        $this->load->view('include/rodape');
    }

    /**
     * Chama as views da página de contato 
     */
    public function contato(){
        $this->load->view('include/cabecalho');
        $this->load->view('include/menu');
        $dados['titulo'] = 'Contate-nos';
        $this->load->view('contato/contato', $dados);
        $this->load->view('include/rodape');
        
    }

    /**
     * Chama as views da página de documentação de pagination 
     */
    public function doc_pagination(){
        $this->load->view('include/cabecalho');
        $this->load->view('include/menu');
        $dados['titulo'] = 'Pagination';
        $this->load->view('doc_componentes/doc_pagination', $dados);
        $this->load->view('include/rodape');
        
    }

    /**
     * Chama as views da página de documentação de navbar 
     */
    public function doc_navbar(){
        $this->load->view('include/cabecalho');
        $this->load->view('include/menu');
        $dados['titulo'] = 'Navbar';
        $this->load->view('doc_componentes/doc_navbar', $dados);
        $this->load->view('include/rodape');
        
    }

    /**
     * Chama as views da página de documentação de collapse 
     */
    public function doc_collapse(){
        $this->load->view('include/cabecalho');
        $this->load->view('include/menu');
        $dados['titulo'] = 'Collapse';
        $this->load->view('doc_componentes/doc_collapse', $dados);
        $this->load->view('include/rodape');
        
    }
}