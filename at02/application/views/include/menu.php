<header>
<nav class="navbar navbar-expand-lg navbar-light fixed-top bg-danger">
  <a class="navbar-brand" href="#">Navbar</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="<?php echo base_url()?>">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Documentação</a>
      <div class="dropdown-menu">
          <a class="dropdown-item" href="<?php echo base_url('doc_pagination')?>">Pagination</a>
          <a class="dropdown-item" href="<?php echo base_url('doc_navbar')?>">Navbar</a>
          <a class="dropdown-item" href="<?php echo base_url('doc_collapse')?>">collapse</a>
      </div>
    </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url('contato')?>">Contato</a>
      </li>
    </ul>
  </div>
</nav>
</header>
