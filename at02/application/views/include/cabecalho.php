<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <title><?php echo $titulo; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.10/styles/agate.min.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/ionicons.min.css') ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/personalizado.css')?>">
        
        
        
    </head>
    <body>
