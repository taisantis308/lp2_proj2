<main role="main">

  <!-- Main jumbotron for a primary marketing message or call to action -->
  <div class="jumbotron">
    <div class="container">
      <h1 class="display-3">Hello, world!</h1>
      <p>Aprenda tudo sobre programação, conheça componentes HTML, entenda sobre responsividade e programação PHP</p>
    
    </div>
  </div>

  <div class="container">
    <!-- Example row of columns -->
    <div class="row">
      <div class="col-md-4">
        <h2>Componente Paginação</h2>
        <p>Conheça o componente que facilita na visualização de grandes conteúdos em páginas de sites, e diminui o tempo de acesso a informações. </p>
        <p><a class="btn btn-secondary bg-danger" href="<?php echo base_url('doc_pagination')?>" role="button">Leia mais... &raquo;</a></p>
      </div>
      <div class="col-md-4">
        <h2>Componente Navbar</h2>
        <p>A barra de navegação é um dos componentes mais importantes de um site, é por meio desse recurso que os usuários buscam de forma mais rápida e assertiva os conteúdos desejados.</p>
        <p><a class="btn btn-secondary bg-danger" href="<?php echo base_url('doc_navbar')?>" role="button">Leia mais... &raquo;</a></p>
      </div>
      <div class="col-md-4">
        <h2>Componente Collapse</h2>
        <p>Entenda como implementar o componente collapse, para personalizar a visualização de componentes em suas páginas.</p>
        <p><a class="btn btn-secondary bg-danger" href="<?php echo base_url('doc_collapse')?>" role="button">Leia mais... &raquo;</a></p>
      </div>
      <div class="collapse" id="collapseExample">
        <div class="card card-body">
            Conheça nossa documentação. Cada documentação é baseada na documentação oficial do Bootstrap e do w3school.
        </div>
        </div>
    </div>

    <hr>

  </div> <!-- /container -->

</main>
